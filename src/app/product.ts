import {Category} from "./category";
import {Brand} from "./brand";

export class Product {
   id:Number;
   model:String;
   price:Number;
   stock:Number;
   category:Category;
   brand:Brand;
}
