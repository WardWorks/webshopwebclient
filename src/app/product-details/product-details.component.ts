import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-product-details',
  templateUrl: 'product-details.components.html',
  styles: [
  ]
})
export class ProductDetailsComponent implements OnInit {

  constructor( private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    console.log( "found id: " + id);
  }

}
